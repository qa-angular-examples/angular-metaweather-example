import { Component, OnInit } from '@angular/core';
import { MetaWeatherService } from '../meta-weather.service';
import { ActivatedRoute } from '@angular/router';
import { WeatherInfo } from '../models/weather-info.model';

import { of, from } from 'rxjs';
import { map, filter, tap, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-weather-day-view-page',
  templateUrl: './weather-day-view-page.component.html',
  styleUrls: ['./weather-day-view-page.component.css']
})
export class WeatherDayViewPageComponent implements OnInit {

  public weatherData: WeatherInfo[] = [];
  public pageTitle: string;

  constructor(
    private metaWeather: MetaWeatherService,
    private aR: ActivatedRoute) { }

  ngOnInit() {
    const { woeid, day, month, year } = this.aR.snapshot.params;
    this.pageTitle = `Data for ${day}-${month}-${year}`;
    this.metaWeather.getWeatherForDate(woeid, year, month, day).subscribe(data => this.weatherData = data);
  }
}
