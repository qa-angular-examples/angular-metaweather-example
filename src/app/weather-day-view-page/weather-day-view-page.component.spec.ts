import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherDayViewPageComponent } from './weather-day-view-page.component';

describe('WeatherDayViewPageComponent', () => {
  let component: WeatherDayViewPageComponent;
  let fixture: ComponentFixture<WeatherDayViewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherDayViewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherDayViewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
