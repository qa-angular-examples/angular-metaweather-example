import { Component, OnInit } from '@angular/core';
import { MetaWeatherService } from '../meta-weather.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationWeather } from '../models/weather.model';
import { WeatherInfo } from '../models/weather-info.model';

@Component({
  selector: 'app-weather-summary-page',
  templateUrl: './weather-summary-page.component.html',
  styleUrls: ['./weather-summary-page.component.css']
})
export class WeatherSummaryPageComponent implements OnInit {

  public weatherData: LocationWeather;
  public today: WeatherInfo;

  constructor(
    private metaWeather: MetaWeatherService,
    private aR: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.metaWeather.getWeatherForWoeid(this.aR.snapshot.params.woeid).subscribe(wData => {
      this.weatherData = wData;
      this.today = wData.consolidated_weather[0];
    });
  }

  onDayClicked(day: WeatherInfo) {
    this.router.navigate([...day.applicable_date.split('-')], { relativeTo: this.aR });
  }

}
