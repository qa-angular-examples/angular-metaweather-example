import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocationWeather } from './models/weather.model';
import { MetaLocation } from './models/meta-location.model';
import { WeatherInfo } from './models/weather-info.model';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MetaWeatherService {

  constructor(private http: HttpClient) { }

  public getLocation(locString: string): Observable<MetaLocation> {
    return this.http.get<MetaLocation[]>('https://www.metaweather.com/api/location/search?query=' + locString)
      .pipe(
        map(x => x[0]),
        map(x => {
          if (x) {
            return x;
          } else {
            throw new Error(`'${locString}' not found. Try again.`);
          }
        })
      );
  }

  public getWeatherForWoeid(woeid: number): Observable<LocationWeather> {
    return this.http.get<LocationWeather>('https://www.metaweather.com/api/location/' + woeid);
  }

  public getWeatherForDate(woeid: number, year: number, month: number, day: number): Observable<WeatherInfo[]> {
    return this.http.get<WeatherInfo[]>(`https://www.metaweather.com/api/location/${woeid}/${year}/${month}/${day}/`);
  }
}
