import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchPageComponent } from './search-page/search-page.component';
import { WeatherSummaryPageComponent } from './weather-summary-page/weather-summary-page.component';
import { WeatherDayViewPageComponent } from './weather-day-view-page/weather-day-view-page.component';


const routes: Routes = [
  {
    path: '',
    component: SearchPageComponent
  },
  {
    path: 'location/:woeid',
    component: WeatherSummaryPageComponent
  },
  {
    path: 'location/:woeid/:year/:month/:day',
    component: WeatherDayViewPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
