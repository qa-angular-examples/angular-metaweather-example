import { Component } from '@angular/core';
import { MetaWeatherService } from '../meta-weather.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent {

  public inputValue = '';

  public errorMessage = '';

  constructor(
    private metaWeather: MetaWeatherService,
    private router: Router
  ) { }

  searchForLocation() {
    this.errorMessage = '';
    if (this.inputValue) {
      this.metaWeather.getLocation(this.inputValue)
        .subscribe(loc => {
          this.router.navigate(['location', loc.woeid]);
        }, err => {
          this.errorMessage = err.message;
        });
    }
  }
}
