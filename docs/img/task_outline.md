# Task

## Outline
Metaweather is an open API for retrieving weather data for locations (https://www.metaweather.com/api/​). Develop a website that interfaces with this API.

The website should include these pages:

 - ### Search
    When the app loads they will be greeted by a search page. The user can enter their location into a search box.

 - ### Forecast
    This page shows the weather for a specific location. It includes the current weather and a day to day breakdown of the weather ahead.

 - ### Weather data
    This page shows detailed information on the weather for a particular day. The API pulls from a number of different sources that should be presented here.

![Metaweather page flow diagram](./metaweather_app_diagram.png)

## Notes
### CORS
Metaweather has a CORS policy in place to prevent it being used in production. To overcome this [disable CORS](https://alfilatov.com/posts/run-chrome-without-cors/) in your browser while developing this website.